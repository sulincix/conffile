# conffile
markup style invented by sulincix
## exclusive characters
|encoded|decoded|event|
|-|-|-|
|&v|,|array seperator|
|&d|{|arrea start|
|&b|}|arrea finish|
|&a|&|escape character|
|&n|:|value name seperator|
## example data type
```
area1{
	data2: hello world, ali&vveli
}
area2{
	subarea1{
		data2: 1&a2, 3&n4
	}
}
```
## usage
|function|event|
|-|-|
|get_area(data,area_name)|select area from name|
|get_value(data,value_name)|select value from name |
|del_areas(data)|remove sub areas|
|decode(data)|decode data|


